﻿#include <SFML/System.hpp>
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>
#include <cmath>
#include <memory>
#include <vector>
#include <random>

namespace mt
{
    inline float DegtoRad(float Deg)
    {
        return 3.14159f / 180.0f * Deg;
    }
    inline float Distance(sf::Vector2f& A, sf::Vector2f& B)
    {
        return std::sqrtf((B.x - A.x) * (B.x - A.x) + (B.y - A.y) * (B.y - A.y));
    }
    inline bool Collides(sf::Vector2f& APos, float ARad, sf::Vector2f& BPos, float BRad)
    {
        return Distance(APos, BPos) < ARad + BRad;
    }
    inline float DotProduct(sf::Vector2f& A, sf::Vector2f& B)
    {
        return (A.x * B.x) + (A.y * B.y);
    }
    inline float SquaredLength(sf::Vector2f& V)
    {
        return DotProduct(V, V);
    }
    inline float Length(sf::Vector2f& V)
    {
        return std::sqrtf(SquaredLength(V));
    }
    sf::Vector2f Normalize(sf::Vector2f& V)
    {
        return V / Length(V);
    }
}

class Circle : public sf::Drawable
{
private:
    sf::Vector2f m_velocity;
    sf::CircleShape m_shape;

public:
    Circle(float Angle, float Speed, sf::Vector2f Position, float Radius, sf::Color FillColor) : m_velocity(), m_shape(Radius, 30)
    {
        m_velocity.x = std::cos(mt::DegtoRad(Angle)) * Speed;
        m_velocity.y = std::sin(mt::DegtoRad(Angle)) * Speed;
        m_shape.setFillColor(FillColor);
        m_shape.setOrigin({ Radius, Radius });
        m_shape.setPosition(Position);
    }
    sf::Vector2f GetVelocity()
    {
        return m_velocity;
    }
    void SetVelocity(sf::Vector2f Velocity)
    {
        m_velocity = Velocity;
    }
    sf::Vector2f GetPosition()
    {
        return m_shape.getPosition();
    }
    void SetPosition(sf::Vector2f Position)
    {
        m_shape.setPosition(Position);
    }
    float GetRadius()
    {
        return m_shape.getRadius();
    }
    void SetRadius(float Radius)
    {
        m_shape.setRadius(Radius);
    }
    virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const
    {
        target.draw(m_shape, states);
    }
    sf::Vector2f GetNextPosition(sf::Time& DeltaTime)
    {
        return GetPosition() + m_velocity * DeltaTime.asSeconds();
    }
};

typedef std::unique_ptr<Circle> CirclePtr;

class Program
{
private:
    std::vector<CirclePtr> m_circles;
    sf::RenderWindow m_window;
    void HandleCollision(CirclePtr& A, CirclePtr& B)
    {
        sf::Vector2f v1 = A->GetVelocity();
        sf::Vector2f v2 = B->GetVelocity();
        sf::Vector2f n = mt::Normalize(v1 - v2);
        float a1 = mt::DotProduct(v1, n);
        float a2 = mt::DotProduct(v2, n);
        float p = (2.0f * (a1 - a2)) / (A->GetRadius() + B->GetRadius());
        v1 -= p * B->GetRadius() * n;
        v2 += p * A->GetRadius() * n;
        A->SetVelocity(v1);
        B->SetVelocity(v2);
    }

public:
    Program() : m_circles(), m_window({ 500, 500 }, "Bouncing Circles", sf::Style::Close | sf::Style::Titlebar, sf::ContextSettings(0, 0, 4, 2, 0))
    {
        m_window.setVerticalSyncEnabled(true);
    }
    void CreateCircles()
    {
        std::mt19937 engine(time(NULL));
        std::uniform_int_distribution<int> color(0, 255);
        std::uniform_int_distribution<int> angle(1, 360);
        std::uniform_int_distribution<int> speed(20, 80);
        std::uniform_int_distribution<int> position(30, 470);
        std::uniform_int_distribution<int> radius(15, 30);
        for (int i = 0; i < 15; i++)
        {
            m_circles.push_back(CirclePtr(new Circle(static_cast<float>(angle(engine)), static_cast<float>(speed(engine)), { static_cast<float>(position(engine)), static_cast<float>(position(engine)) }, static_cast<float>(radius(engine)), { static_cast<uint8_t>(color(engine)), static_cast<uint8_t>(color(engine)), static_cast<uint8_t>(color(engine)) })));
            CirclePtr& circle = m_circles.back();
            bool collides = true;
            while (collides)
            {
                collides = false;
                for (auto& circ : m_circles)
                {
                    if (circ == circle) continue;
                    if (mt::Collides(circ->GetPosition(), circ->GetRadius(), circle->GetPosition(), circle->GetRadius()))
                    {
                        collides = true;
                        circle->SetPosition({ static_cast<float>(position(engine)), static_cast<float>(position(engine)) });
                        break;
                    }
                }
            }
        }
    }
    void RunLoop()
    {
        sf::Clock frameclock;
        sf::Time elapsedtime(sf::Time::Zero);
        sf::Time timestep(sf::seconds(1.0f / 45.0f));
        while (m_window.isOpen())
        {
            sf::Event evt;
            while (m_window.pollEvent(evt))
            {
                if (evt.type == sf::Event::Closed) m_window.close();
            }

            elapsedtime += frameclock.restart();

            while (elapsedtime >= timestep)
            {
                elapsedtime -= timestep;

                for (auto& circle : m_circles)
                {
                    sf::Vector2f pos = circle->GetNextPosition(timestep);
                    bool collides = false;
                    for (auto& circ : m_circles)
                    {
                        if (circ == circle) continue;
                        if (mt::Collides(pos, circle->GetRadius(), circ->GetPosition(), circ->GetRadius()))
                        {
                            collides = true;
                            HandleCollision(circle, circ);
                            break;
                        }
                    }
                    if (!collides)
                    {
                        if (pos.x < circle->GetRadius() || pos.x > 500 - circle->GetRadius())
                        {
                            circle->SetVelocity({ -circle->GetVelocity().x, circle->GetVelocity().y });
                        }
                        else if (pos.y < circle->GetRadius() || pos.y > 500 - circle->GetRadius())
                        {
                            circle->SetVelocity({ circle->GetVelocity().x, -circle->GetVelocity().y });
                        }
                        else
                        {
                            circle->SetPosition(pos);
                        }
                    }
                }
            }

            m_window.clear();
            for (auto& circle : m_circles)
            {
                m_window.draw(*circle);
            }
            m_window.display();
        }
    }
};

int main()
{
    Program program;
    program.CreateCircles();
    program.RunLoop();
}