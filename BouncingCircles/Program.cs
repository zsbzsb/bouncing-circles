﻿using System;
using System.Collections.Generic;
using SFML.Window;
using SFML.Graphics;
using NetEXT.Vectors;
using NetEXT.MathFunctions;
using NetEXT.TimeFunctions;

namespace BouncingCircles
{
    class Program
    {
        static void Main(string[] args)
        {
            // window initialization
            RenderWindow window = new RenderWindow(new VideoMode(500, 500), "Bouncing Circles", Styles.Close | Styles.Titlebar, new ContextSettings(0, 0, 4));
            window.Closed += (sender, e) => { window.Close(); }; // Close window when closed event fires
            window.SetVerticalSyncEnabled(true);

            // circle initialization
            List<Circle> circles = new List<Circle>(); // create list (same as vector) for holding circles
            for (int i = 0; i < 15; i++) // create 15 circles
            {
                // generate circle properties
                float angle = RandomGenerator.Random(1, 360); // random angle in degrees [1, 360]
                float speed = RandomGenerator.Random(20, 80); // random speed in pixels per second [20, 80]
                float x = RandomGenerator.Random(30, 470); // random x starting position [30, 470]
                float y = RandomGenerator.Random(30, 470); // random y starting position [30, 470]
                float radius = RandomGenerator.Random(15, 30); // random size [15, 30]
                Color color = new Color((byte)RandomGenerator.Random(0, 255), (byte)RandomGenerator.Random(0, 255), (byte)RandomGenerator.Random(0, 255)); // random color

                // initialize circle and add to list
                Circle circle = new Circle(angle, speed, new Vector2f(x, y), radius, color);
                circles.Add(circle);

                // ensure circle is not overlapping
                // do this by looping over previous circles and check for collision
                // if collision is found then generate a new position
                bool collides = true;
                while (collides)
                {
                    collides = false;
                    foreach (var circ in circles)
                    {
                        if (circ == circle) continue; // don't check for collision against the same circle
                        if (Collides(circ.Position, circ.Radius, circle.Position, circle.Radius))
                        {
                            collides = true;
                            x = RandomGenerator.Random(30, 470);
                            y = RandomGenerator.Random(30, 470);
                            circle.Position = new Vector2f(x, y);
                            break;
                        }
                    }
                }
            }

            // time measurement initialization
            Clock frameclock = new Clock();
            Time elapsedtime = Time.Zero;
            Time timestep = Time.FromSeconds(1f / 45f);

            while (window.IsOpen()) // loop while window is open
            {
                window.DispatchEvents(); // handle events

                elapsedtime += frameclock.Restart(); // increment elapsed time with the previous frame time

                while (elapsedtime >= timestep) // fixed time step - loop while enough time has passed to update physics
                {
                    elapsedtime -= timestep;

                    // now for the physics
                    foreach (var circle in circles) // loop through all circles and update them
                    {
                        Vector2f pos = circle.GetNextPosition(timestep); // gets the new position
                        bool collides = false;
                        foreach (var circ in circles) // loop through other circles and calculate collisions
                        {
                            if (circ == circle) continue; // don't check for collision against ourself
                            if (Collides(pos, circle.Radius, circ.Position, circ.Radius))
                            {
                                // collision happened
                                collides = true;
                                // change circles velocity
                                HandleCollision(circle, circ);
                                break;
                            }
                        }
                        if (!collides)
                        {
                            // check for collision on the left or right wall
                            if (pos.X < circle.Radius || pos.X > 500 - circle.Radius)
                            {
                                circle.Velocity = new Vector2f(-circle.Velocity.X, circle.Velocity.Y);
                            }
                            // check for collision on the top or bottom wall
                            else if (pos.Y < circle.Radius || pos.Y > 500 - circle.Radius)
                            {
                                circle.Velocity = new Vector2f(circle.Velocity.X, -circle.Velocity.Y);
                            }
                            else
                            {
                                circle.Position = pos; // no collision happened so move the circle
                            }
                        }
                    }
                }

                // render circles
                window.Clear();
                foreach (var circle in circles) // loop and draw each circle
                {
                    window.Draw(circle);
                }
                window.Display();
            }
        }
        private static float Distance(Vector2f A, Vector2f B) // returns distance between two points
        {
            return (float)Trigonometry.SqrRoot((B.X - A.X) * (B.X - A.X) + (B.Y - A.Y) * (B.Y - A.Y));
        }
        private static bool Collides(Vector2f APos, float ARad, Vector2f BPos, float BRad) // simple collision detection between two circles
        {
            float dist = Distance(APos, BPos); // gets distance between circles
            return dist < ARad + BRad; // if dist < combined radius we have a collision
        }
        private static Vector2f Normalize(Vector2f V)
        {
            return V / Vector2Algebra.Length(V);
        }
        private static void HandleCollision(Circle A, Circle B)
        {
            Vector2f v1 = A.Velocity;
            Vector2f v2 = B.Velocity;
            Vector2f n = Normalize(v1 - v2);
            float a1 = Vector2Algebra.DotProduct(v1, n);
            float a2 = Vector2Algebra.DotProduct(v2, n);
            float p = (2f * (a1 - a2)) / (A.Radius + B.Radius);
            v1 -= p * B.Radius * n;
            v2 += p * A.Radius * n;
            A.Velocity = v1;
            B.Velocity = v2;
        }
    }
    class Circle : Drawable // simple class for storing and drawing a circle
    {
        #region Variables
        private Vector2f _vector;
        private CircleShape _shape;
        #endregion

        #region Properties
        public Vector2f Velocity
        {
            get
            {
                return _vector;
            }
            set
            {
                _vector = value;
            }
        }
        public Vector2f Position
        {
            get
            {
                return _shape.Position;
            }
            set
            {
                _shape.Position = value;
            }
        }
        public float Radius
        {
            get
            {
                return _shape.Radius;
            }
        }
        #endregion

        #region Constructors
        public Circle(float Angle, float Speed, Vector2f Position, float Radius, Color FillColor)
        {
            _vector = new PolarVector(Speed, Angle);
            _shape = new CircleShape(Radius, 30) { FillColor = FillColor, Origin = new Vector2f(Radius, Radius), Position = Position };
        }
        #endregion

        #region Functions
        public void Draw(RenderTarget Target, RenderStates States)
        {
            Target.Draw(_shape, States);
        }
        public Vector2f GetNextPosition(Time DeltaTime)
        {
            return Position + _vector * (float)DeltaTime.Seconds;
        }
        #endregion
    }
}
